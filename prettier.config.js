const options = {
    singleQuote: true,
    tabWidth: 4,
    trailingComma: 'all',
    printwidth: 120,
};

module.exports = options;